package app.views;

import app.entities.Post;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;

public class ApiPostView implements ApiViewInterface<Post> {
    @JsonProperty("id")
    private String id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("date")
    private String date;
    @JsonProperty("description")
    private String description;
    @JsonProperty("image")
    private String image;

    @Override
    public void initVeiw(Post entity) {
        setId(entity.getId());
        setTitle(entity.getTitle());
        setDate(entity.getDate());
        setDescription(entity.getDescription());
        setImage(entity.getImage());
    }

    @Override
    public HashMap<String, Object> getView() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", getId());
        result.put("title", getTitle());
        result.put("date", getDate());
        result.put("description", getDescription());
        result.put("image", getImage());
        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
