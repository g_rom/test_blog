package app.views;

import java.util.HashMap;

public interface ApiViewInterface<T> {
    void initVeiw(T entity);

    HashMap<String, Object> getView();
}
