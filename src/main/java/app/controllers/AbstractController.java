package app.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.el.PropertyNotFoundException;
import java.io.IOException;

@ControllerAdvice
public abstract class AbstractController extends RuntimeException {
    protected ModelAndView model = new ModelAndView();

    protected String layout = "index";
    protected String layoutError = "error";

    public AbstractController() {
        model.setViewName(layout);
    }

    protected void setDirectoryLayout(String layout) {
        this.layout = layout;
    }

    protected void setErrorLayout(String layout) {
        this.layoutError = layout;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public Object noFoundException(NoHandlerFoundException ex) {
        ModelAndView model = new ModelAndView();
        model.setViewName(layoutError);
        model.addObject("error", ex);
        model.addObject("page_name", "404");
        return model;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({
            RuntimeException.class,
            PropertyNotFoundException.class,
            Exception.class,
            MultipartException.class,
            IOException.class
    })
    public Object internalServerException(Exception ex) {
        ModelAndView model = new ModelAndView();
        model.setViewName(layoutError);
        model.addObject("error", ex);
        model.addObject("page_name", "500");
        return model;
    }
}
