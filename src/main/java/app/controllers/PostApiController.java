package app.controllers;

import app.entities.Post;
import app.services.FileService;
import app.views.ApiAnswer;
import app.views.ApiPostView;
import app.views.forms.ApiPostForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping(value = "/api", method = {RequestMethod.GET, RequestMethod.POST})
public class PostApiController extends AbstractRestController {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private FileService fileService;

    @RequestMapping(value = "/get-list", method = RequestMethod.POST)
    public ApiAnswer getList(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {
        HashMap<String, Object> result = new HashMap<>();

        List<Post> postList = mongoTemplate.findAll(Post.class);

        result.clear();
        List<HashMap> postViews = new ArrayList<>();
        for (Post post : postList) {
            ApiPostView postView = new ApiPostView();
            postView.initVeiw(post);
            postViews.add(postView.getView());
        }
        result.put("posts", postViews);
        prepareAnswer(response, HttpStatus.OK, HttpServletResponse.SC_OK, result);

        return answer;
    }

    @RequestMapping(value = "/get-post", method = RequestMethod.POST)
    public ApiAnswer getPost(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {
        Properties data = new Properties();
        HashMap<String, Object> result = new HashMap<>();

        try {
            data = getRequestData(request);

            Query query = new Query(Criteria.where("_id").is(data.getProperty("id")));
            List<Post> postList = mongoTemplate.find(query, Post.class, "post");

            result.clear();
            List<HashMap> postViews = new ArrayList<>();
            for (Post post : postList) {
                ApiPostView postView = new ApiPostView();
                postView.initVeiw(post);
                postViews.add(postView.getView());
            }
            result.put("posts", postViews);
            prepareAnswer(response, HttpStatus.OK, HttpServletResponse.SC_OK, result);
        } catch (IOException error) {
            result.clear();
            result.put("error", error.getMessage());
            prepareAnswer(response, HttpStatus.INTERNAL_SERVER_ERROR, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, result);
        }

        return answer;
    }

    @RequestMapping(value = "/add-post", method = RequestMethod.POST)
    public ApiAnswer addPost(
            @ModelAttribute("data") ApiPostForm data,
            HttpServletResponse response,
            HttpServletRequest request,
            HttpSession httpSession) {

        Post post = new Post();
        post.setId(UUID.randomUUID().toString());
        post.setTitle(data.getTitle());
        post.setDescription(data.getDescription());

        if (data.getImage() != null) {
            post.setImage(fileService.saveFile(data.getImage(), "post/" + post.getId() + "/"));
        }

        mongoTemplate.save(post);
        ApiPostView postView = new ApiPostView();
        postView.initVeiw(post);
        prepareAnswer(response, HttpStatus.OK, HttpServletResponse.SC_OK, postView.getView());

        return answer;
    }
}
