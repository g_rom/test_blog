package app.entities;

import org.springframework.data.annotation.Id;

import java.io.Serializable;

public class Post implements Serializable {
    @Id
    private String id;

    private String title;
    private String description;
    private String date;
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return String.format("Post[id=%s, title='%s', date='%s', description='%s, image='%s']", id, title, date, description, image);
    }
}
